<?
namespace Articul\Checklist;
use Bitrix\Main\Loader;

IncludeModuleLangFile(__FILE__);

class Test
{
    /*
     * event handler
    */

    static public function onCheckListGet()
    {
        $arCheckList = array(
            "CATEGORIES" => array(), 
            "POINTS" => array()
        );

        $arTests = array(
            "ARTICUL_DEV" => array(
                "NAME" => GetMessage("articul_checklist_ARTICUL_GROUP_NAME_DEV")
            ),
            "ARTICUL_PROD" => array(
                "NAME" => GetMessage("articul_checklist_ARTICUL_GROUP_NAME_PROD")
            ),

            "DEV_ARCHITECTURE" => array(
                "NAME" => GetMessage("articul_checklist_ARTICUL_GROUP_NAME_ARCHITECTURE"),
                "PARENT" => "ARTICUL_DEV",
                "TESTS" => array(
                    "AM.ARCH.001" => array(
                        "REQUIRE" => "Y",
                        "AUTO" => "Y",
                        "CLASS_NAME" => __CLASS__,
                        "METHOD_NAME" => "CheckArchGitSettings",
                        "NAME" => GetMessage("articul_checklist_AM.ARCH.001_NAME"),
                        "DESC" => GetMessage("articul_checklist_AM.ARCH.001_DESC"),
                        "HOWTO" => GetMessage("articul_checklist_AM.ARCH.001_HOWTO"),
                    ),
                    "AM.ARCH.002" => array(
                        "REQUIRE" => "Y",
                        "AUTO" => "Y",
                        "CLASS_NAME" => __CLASS__,
                        "METHOD_NAME" => "CheckArchCoreGitIgnored",
                        "NAME" => GetMessage("articul_checklist_AM.ARCH.002_NAME"),
                        "DESC" => GetMessage("articul_checklist_AM.ARCH.002_DESC"),
                        "HOWTO" => GetMessage("articul_checklist_AM.ARCH.002_HOWTO"),
                    ),
                    "AM.ARCH.003" => array(
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.ARCH.003_NAME"),
                    ),
                    "AM.ARCH.004" => array(
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.ARCH.004_NAME"),
                    ),
                    "AM.ARCH.005" => array(
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.ARCH.005_NAME"),
                        "DESC" => GetMessage("articul_checklist_AM.ARCH.005_DESC"),
                    ),
                    "AM.ARCH.006" => array(
                        "REQUIRE" => "Y",
                        "AUTO" => "Y",
                        "CLASS_NAME" => __CLASS__,
                        "METHOD_NAME" => "CheckArchFolderLocal",
                        "NAME" => GetMessage("articul_checklist_AM.ARCH.006_NAME"),
                        "HOWTO" => GetMessage("articul_checklist_AM.ARCH.006_HOWTO"),
                    ),
                ),
            ),
            "DEV_CACHE" => array(
                "NAME" => GetMessage("articul_checklist_ARTICUL_GROUP_NAME_CACHE"),
                "PARENT" => "ARTICUL_DEV",
                "TESTS" => array(
                    "AM.CACHE.001" => array(
                        "REQUIRE" => "Y",
                        "AUTO" => "Y",
                        "CLASS_NAME" => __CLASS__,
                        "METHOD_NAME" => "CheckCacheAutoOn",
                        "NAME" => GetMessage("articul_checklist_AM.CACHE.001_NAME"),
                        "DESC" => GetMessage("articul_checklist_AM.CACHE.001_DESC"),
                    ),
                    "AM.CACHE.002" => array(
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.CACHE.002_NAME"),
                    ),
                    "AM.CACHE.003" => array(
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.CACHE.003_NAME"),
                    ),
                ),
            ),
            "DEV_CONTENT" => array(
                "NAME" => GetMessage("articul_checklist_ARTICUL_GROUP_NAME_CONTENT"),
                "PARENT" => "ARTICUL_DEV",
                "TESTS" => array(
                    "AM.CONT.001" => array(
                        "PARENT" => "DEV_CONTENT",
                        "REQUIRE" => "Y",
                        "AUTO" => "Y",
                        "CLASS_NAME" => __CLASS__,
                        "METHOD_NAME" => "CheckContentRobotsTxt",
                        "NAME" => GetMessage("articul_checklist_AM.CONT.001_NAME"),
                        "DESC" => GetMessage("articul_checklist_AM.CONT.001_DESC"),
                        "HOWTO" => GetMessage("articul_checklist_AM.CONT.001_HOWTO"),
                    ),
                    "AM.CONT.002" => array(
                        "PARENT" => "DEV_CONTENT",
                        "REQUIRE" => "Y",
                        "AUTO" => "Y",
                        "CLASS_NAME" => __CLASS__,
                        "METHOD_NAME" => "CheckContentFaviconIco",
                        "NAME" => GetMessage("articul_checklist_AM.CONT.002_NAME"),
                        "DESC" => GetMessage("articul_checklist_AM.CONT.002_DESC"),
                        "HOWTO" => GetMessage("articul_checklist_AM.CONT.002_HOWTO"),
                    ),
                    "AM.CONT.003" => array(
                        "PARENT" => "DEV_CONTENT",
                        "REQUIRE" => "Y",
                        "AUTO" => "Y",
                        "CLASS_NAME" => __CLASS__,
                        "METHOD_NAME" => "CheckContentAppleTouchIcon",
                        "NAME" => GetMessage("articul_checklist_AM.CONT.003_NAME"),
                        "DESC" => GetMessage("articul_checklist_AM.CONT.003_DESC"),
                        "HOWTO" => GetMessage("articul_checklist_AM.CONT.003_HOWTO"),
                        "LINKS" => ""
                    ),
                    "AM.CONT.004" => array(
                        "PARENT" => "DEV_CONTENT",
                        "REQUIRE" => "Y",
                        "AUTO" => "Y",
                        "CLASS_NAME" => __CLASS__,
                        "METHOD_NAME" => "CheckContentHtmlLangAttr",
                        "NAME" => GetMessage("articul_checklist_AM.CONT.004_NAME"),
                    ),
                    "AM.CONT.005" => array(
                        "PARENT" => "DEV_CONTENT",
                        "REQUIRE" => "Y",
                        "AUTO" => "Y",
                        "CLASS_NAME" => __CLASS__,
                        "METHOD_NAME" => "CheckContentOgTags",
                        "NAME" => GetMessage("articul_checklist_AM.CONT.005_NAME"),
                        "DESC" => GetMessage("articul_checklist_AM.CONT.005_DESC"),
                    ),
                    "AM.CONT.006" => array(
                        "PARENT" => "DEV_CONTENT",
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.CONT.006_NAME"),
                    ),
                    "AM.CONT.007" => array(
                        "PARENT" => "DEV_CONTENT",
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.CONT.007_NAME"),
                    ),
                ),
            ),
            "DEV_SECURITY" => array(
                "NAME" => GetMessage("articul_checklist_ARTICUL_GROUP_NAME_SECURITY"),
                "PARENT" => "ARTICUL_DEV",
                "TESTS" => array(
                    "AM.SECR.001" => array(
                        "PARENT" => "DEV_SECURITY",
                        "REQUIRE" => "Y",
                        "AUTO" => "Y",
                        "CLASS_NAME" => __CLASS__,
                        "METHOD_NAME" => "CheckSecurityGitConfigAccess",
                        "NAME" => GetMessage("articul_checklist_AM.SECR.001_NAME"),
                        "DESC" => GetMessage("articul_checklist_AM.SECR.001_DESC"),
                    ),
                    "AM.SECR.002" => array(
                        "PARENT" => "DEV_SECURITY",
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.SECR.002_NAME"),
                        "HOWTO" => GetMessage("articul_checklist_AM.SECR.002_HOWTO"),
                    ),
                    "AM.SECR.003" => array(
                        "PARENT" => "DEV_SECURITY",
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.SECR.003_NAME"),
                        "HOWTO" => GetMessage("articul_checklist_AM.SECR.003_HOWTO"),
                    ),
                    "AM.SECR.004" => array(
                        "PARENT" => "DEV_SECURITY",
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.SECR.004_NAME"),
                    ),
                    "AM.SECR.005" => array(
                        "PARENT" => "DEV_SECURITY",
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.SECR.005_NAME"),
                    ),
                ),
            ),
            "DEV_WIKI" => array(
                "NAME" => GetMessage("articul_checklist_ARTICUL_GROUP_NAME_WIKI"),
                "PARENT" => "ARTICUL_DEV",
                "TESTS" => array(
                    "AM.WIKI.001" => array(
                        "PARENT" => "DEV_WIKI",
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.WIKI.001_NAME"),
                    ),
                    "AM.WIKI.002" => array(
                        "PARENT" => "DEV_WIKI",
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.WIKI.002_NAME"),
                    ),
                    "AM.WIKI.003" => array(
                        "PARENT" => "DEV_WIKI",
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.WIKI.003_NAME"),
                    ),
                ),
            ),
            "DEV_COMMON" => array(
                "NAME" => GetMessage("articul_checklist_ARTICUL_GROUP_NAME_COMMON"),
                "PARENT" => "ARTICUL_DEV",
                "TESTS" => array(
                    "AM.COMN.001" => array(
                        "PARENT" => "DEV_COMMON",
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.COMN.001_NAME"),
                        "HOWTO" => GetMessage("articul_checklist_AM.COMN.001_HOWTO"),
                    ),
                ),
            ),
            "DEV_SEO" => array(
                "NAME" => GetMessage("articul_checklist_ARTICUL_GROUP_NAME_SEO"),
                "PARENT" => "ARTICUL_DEV",
                "TESTS" => array(
                    "AM.SEO.001" => array(
                        "PARENT" => "DEV_SEO",
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.SEO.001_NAME"),
                        "HOWTO" => GetMessage("articul_checklist_AM.SEO.001_HOWTO"),
                    ),
                    "AM.SEO.002" => array(
                        "PARENT" => "DEV_SEO",
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.SEO.002_NAME"),
                        "DESC" => GetMessage("articul_checklist_AM.SEO.002_DESC"),
                    ),
                ),
            ),
            "PROD_COMMON" => array(
                "NAME" => GetMessage("articul_checklist_ARTICUL_GROUP_NAME_COMMON"),
                "PARENT" => "ARTICUL_PROD",
                "TESTS" => array(
                    "AM.COMN.101" => array(
                        "PARENT" => "PROD_COMMON",
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.COMN.101_NAME"),
                        "HOWTO" => GetMessage("articul_checklist_AM.COMN.101_HOWTO"),
                    ),
                    "AM.COMN.102" => array(
                        "PARENT" => "PROD_COMMON",
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.COMN.102_NAME"),
                        "DESC" => GetMessage("articul_checklist_AM.COMN.102_DESC"),
                    ),
                    "AM.COMN.103" => array(
                        "PARENT" => "PROD_COMMON",
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.COMN.103_NAME"),
                        "HOWTO" => GetMessage("articul_checklist_AM.COMN.103_HOWTO"),
                    ),
                    "AM.COMN.104" => array(
                        "PARENT" => "PROD_COMMON",
                        "REQUIRE" => "Y",
                        "AUTO" => "Y",
                        "CLASS_NAME" => __CLASS__,
                        "METHOD_NAME" => "CheckCommonAutoBackupEnabled",
                        "NAME" => GetMessage("articul_checklist_AM.COMN.104_NAME"),
                        "DESC" => GetMessage("articul_checklist_AM.COMN.104_DESC"),
                    ),
                    "AM.COMN.105" => array(
                        "PARENT" => "PROD_COMMON",
                        "REQUIRE" => "Y",
                        "AUTO" => "Y",
                        "CLASS_NAME" => __CLASS__,
                        "METHOD_NAME" => "CheckCommonMonitoringEnabled",
                        "NAME" => GetMessage("articul_checklist_AM.COMN.105_NAME"),
                        "DESC" => GetMessage("articul_checklist_AM.COMN.105_DESC"),
                    ),
                ),
            ),
            "PROD_HOSTING" => array(
                "NAME" => GetMessage("articul_checklist_ARTICUL_GROUP_NAME_HOSTING"),
                "PARENT" => "ARTICUL_PROD",
                "TESTS" => array(
                    "AM.HOST.101" => array(
                        "REQUIRE" => "Y",
                        "AUTO" => "Y",
                        "CLASS_NAME" => __CLASS__,
                        "METHOD_NAME" => "CheckHostingGitInstalled",
                        "NAME" => GetMessage("articul_checklist_AM.HOST.101_NAME"),
                        "HOWTO" => GetMessage("articul_checklist_AM.HOST.101_HOWTO"),
                    ),
                    "AM.HOST.102" => array(
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.HOST.102_NAME"),
                    ),
                    "AM.HOST.103" => array(
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.HOST.103_NAME"),
                        "HOWTO" => GetMessage("articul_checklist_AM.HOST.103_HOWTO"),
                    ),
                    "AM.HOST.104" => array(
                        "PARENT" => "PROD_HOSTING",
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.HOST.104_NAME"),
                        "DESC" => GetMessage("articul_checklist_AM.HOST.104_DESC"),
                        "HOWTO" => GetMessage("articul_checklist_AM.HOST.104_HOWTO"),
                    ),
                    "AM.HOST.105" => array(
                        "PARENT" => "PROD_HOSTING",
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.HOST.105_NAME"),
                        "DESC" => GetMessage("articul_checklist_AM.HOST.105_DESC"),
                        "HOWTO" => GetMessage("articul_checklist_AM.HOST.105_HOWTO"),
                    ),
                ),
            ),
            "PROD_SECURITY" => array(
                "NAME" => GetMessage("articul_checklist_ARTICUL_GROUP_NAME_SECURITY"),
                "PARENT" => "ARTICUL_PROD",
                "TESTS" => array(
                    "AM.SECR.101" => array(
                        "PARENT" => "PROD_SECURITY",
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.SECR.101_NAME"),
                        "HOWTO" => GetMessage("articul_checklist_AM.SECR.101_HOWTO"),
                    ),
                ),
            ),
            "PROD_MONITORING" => array(
                "NAME" => GetMessage("articul_checklist_ARTICUL_GROUP_NAME_QUALITY"),
                "PARENT" => "ARTICUL_PROD",
                "TESTS" => array(
                    "AM.QMON.101" => array(
                        "PARENT" => "PROD_MONITORING",
                        "REQUIRE" => "Y",
                        "AUTO" => "N",
                        "NAME" => GetMessage("articul_checklist_AM.QMON.101_NAME"),
                    ),
                ),
            ),
        );

        foreach ($arTests as $catKey => $arCat)
        {
            if (!$catKey)
            {
                continue;
            }

            $arCheckList["CATEGORIES"][$catKey] = array(
                "NAME" => $arCat["NAME"],
                "PARENT" => $arCat["PARENT"],
            );

            if (is_array($arCat["TESTS"]))
            {
                foreach ($arCat["TESTS"] as $testKey => $arTest)
                {
                    $arTest["PARENT"] = $catKey;
                    $arCheckList["POINTS"][$testKey] = $arTest;
                }
            }
        }

        return $arCheckList;
    }

    /*
     * helpers
    */

    static private function GetSuccessResult($arParams = array())
    {
        $arResult = array(
            "STATUS" => true,
            "MESSAGE" => array(
                "PREVIEW" => $arParams["PREVIEW"] ?: GetMessage("articul_checklist_TEST_SUCCESS"),
                "DETAIL" => $arParams["DETAIL"] ?: false,
            ),
        );
        return $arResult;
    }

    static private function GetFailedResult($arParams = array())
    {
        $arResult = array(
            "STATUS" => false,
            "MESSAGE" => array(
                "PREVIEW" => $arParams["PREVIEW"] ?: GetMessage("articul_checklist_TEST_FAIL"),
                "DETAIL" => $arParams["DETAIL"] ?: false,
            ),
        );
        return $arResult;
    }

    static private function GetSites()
    {
        $arSites = array();
        $rsSites = \CSite::GetList($by="sort", $order="desc", Array("ACTIVE" => "Y"));
        while ($arSite = $rsSites->Fetch())
        {
            $arSite["SERVER_NAME"] = strtolower($arSite["SERVER_NAME"]);
            $arSites[] = $arSite;
        }
        return $arSites;
    }

    /*
     * auto tests
    */

    static public function CheckArchGitSettings()
    {
        if (file_exists($_SERVER["DOCUMENT_ROOT"]."/.git/config")) {
            $gitConfig = file_get_contents($_SERVER["DOCUMENT_ROOT"]."/.git/config");
        }
        $search = "[remote";

        if (!$gitConfig)
        {
            return static::GetFailedResult(array(
                "PREVIEW" => GetMessage("articul_checklist_AM.ARCH.001_NOT_FOUND")
            ));
        }

        if (strpos($gitConfig, $search) === false)
        {
            return static::GetFailedResult();
        }

        return static::GetSuccessResult();
    }

    static public function CheckArchCoreGitIgnored()
    {
        if (file_exists($_SERVER["DOCUMENT_ROOT"]."/.gitignore")) {
            $arGitIgnore = file($_SERVER["DOCUMENT_ROOT"]."/.gitignore");
        }
        $arSearch = array("bitrix", "upload", ".htaccess");
        $arErrors = array(
            "bitrix" => true,
            "upload" => true,
            ".htaccess" => true
        );

        if (!$arGitIgnore)
        {
            return static::GetFailedResult(array(
                "PREVIEW" => GetMessage("articul_checklist_AM.ARCH.002_NOT_FOUND")
            ));
        }

        foreach ($arGitIgnore as $str)
        {
            $str = trim(strtolower(str_replace("/", "", $str)));
            if(in_array($str, $arSearch))
            {
                unset($arErrors[$str]);
            }
        }

        if (!empty($arErrors))
        {
            return static::GetFailedResult();
        }

        return static::GetSuccessResult();
    }

    static public function CheckCacheAutoOn()
    {
        $cacheOn = \COption::GetOptionString("main", "component_cache_on", "Y") == "N";

        if ($cacheOn == "N")
        {
            return static::GetFailedResult();
        }

        return static::GetSuccessResult();
    }

    static public function CheckContentRobotsTxt()
    {
        $robotsTxtPath = $_SERVER["DOCUMENT_ROOT"]."/robots.txt";

        if (!file_exists($robotsTxtPath))
        {
            return static::GetFailedResult();
        }

        return static::GetSuccessResult();
    }

    static public function CheckContentFaviconIco()
    {
        $faviconIcoPath = $_SERVER["DOCUMENT_ROOT"]."/favicon.ico";

        if (!file_exists($faviconIcoPath))
        {
            return static::GetFailedResult();
        }

        return static::GetSuccessResult();
    }

    static public function CheckContentAppleTouchIcon()
    {
        $arSites = static::GetSites();
        $bSuccess = true;
        $detailText = "";

        foreach ($arSites as $arSite)
        {
            $detailText .= GetMessage("articul_checklist_COMMON_SITE_CHECK", array(
                "#SITE#" => $arSite["ID"]
            ));
            $detailText .= "<ul>";

            $appleIconPath = $arSite["ABS_DOC_ROOT"].$arSite["DIR"]."apple-touch-icon.png";
            if (file_exists($appleIconPath))
            {
                $detailText .= "<li>".GetMessage("articul_checklist_COMMON_FILE_FOUND", array(
                        "#FILE#" => $appleIconPath
                ))."</li>";
                $detailText .= "</ul>";
                continue;
            }

            $appleIconPath = $arSite["ABS_DOC_ROOT"].$arSite["DIR"]."apple-touch-icon-precomposed.png";
            if (file_exists($appleIconPath))
            {
                $detailText .= "<li>".GetMessage("articul_checklist_COMMON_FILE_FOUND", array(
                    "#FILE#" => $appleIconPath
                ))."</li>";
                $detailText .= "</ul>";
                continue;
            }

            if (!$arSite["SERVER_NAME"]) {
                $bSuccess = false;
                $detailText .= "<li>" . GetMessage("articul_checklist_SERVER_NAME_NOT_FOUND")
                    . "<a target=\"_blank\" " . "href=\"/bitrix/admin/site_edit.php?LID=" . $arSite['LID'] . "\">"
                    . GetMessage("articul_checklist_SERVER_URL") . "</a></span>" . "</li>";
                $detailText .= "</ul>";
                continue;
            }

            $siteUrl = "http://".$arSite["SERVER_NAME"].$arSite["DIR"];

            $headers = @get_headers($siteUrl);
            if (strpos($headers[0],'200') == false) {
                $bSuccess = false;
                $detailText .= "<li>" . GetMessage("articul_checklist_DOMEN")
                    . "&nbsp;<a target=\"_blank\" " . "href=\"$siteUrl\">"
                    . $siteUrl . "</a>&nbsp;" . GetMessage("articul_checklist_NOT_FOUND")  . "</span>" . "</li>";
                $detailText .= "</ul>";
                continue;
            }

            $detailText .= "<li>".GetMessage("articul_checklist_COMMON_URL_CHECK", array(
                "#URL#" => $siteUrl
            ))."</li>";

            $mainPageContent = file_get_contents($siteUrl);
            preg_match_all('/<head(.*)<\/head>/s', $mainPageContent, $matchesHeader);
            $mainPageHeader = $matchesHeader[1][0];

            if (!$mainPageHeader)
            {
                $bSuccess = false;
                $detailText .= "<li>".GetMessage("articul_checklist_COMMON_PAGE_NOT_AVAILABLE")."</li>";
                $detailText .= "</ul>";
                continue;
            }

            $arFiles = array();

            preg_match_all("/<link.*?rel=\"apple-touch-icon\".*?href=\"(.*?)\"/s", $mainPageHeader, $matchesDefaultIcon);
            if (count($matchesDefaultIcon[1]))
            {
                $arFiles = array_merge($arFiles, $matchesDefaultIcon[1]);
            }

            preg_match_all("/<link.*?rel=\"apple-touch-icon-precomposed\".*?href=\"(.*?)\"/s", $mainPageHeader, $matchesPrecomposedIcon);
            if (count($matchesPrecomposedIcon[1]))
            {
                $arFiles = array_merge($arFiles, $matchesPrecomposedIcon[1]);
            }

            if (!count($arFiles))
            {
                $bSuccess = false;
                $detailText .= "<li>".GetMessage("articul_checklist_AM.CONT.003_NO_TAG")."</li>";
                $detailText .= "</ul>";
                continue;
            }

            foreach ($arFiles as $fileUrl)
            {
                if (strpos($fileUrl, "://") === false) 
                {
                    $fileUrl = "http://".$arSite["SERVER_NAME"].$fileUrl;
                }

                $responseHeaders = @get_headers($fileUrl);
                if (strpos($responseHeaders[0], "200 OK") !== false)
                {
                    $detailText .= "<li>".GetMessage("articul_checklist_COMMON_FILE_FOUND", array(
                        "#FILE#" => $fileUrl
                    ))."</li>";
                }
                else
                {
                    $bSuccess = false;
                    $detailText .= "<li>".GetMessage("articul_checklist_COMMON_FILE_NOT_FOUND", array(
                        "#FILE#" => $fileUrl
                    ))."</li>";
                }
            }

            $detailText .= "</ul>";
        }

        $detailText .= "<p>".GetMessage("articul_checklist_AM.CONT.003_MORE_INFO")."</p>";

        if (!$bSuccess)
        {

            return static::GetFailedResult(array(
                "DETAIL" => $detailText
            ));
        }

        return static::GetSuccessResult(array(
            "DETAIL" => $detailText
        ));
    }

    static public function CheckContentHtmlLangAttr()
    {
        $arSites = static::GetSites();
        $bSuccess = true;
        $detailText = "";

        foreach ($arSites as $arSite)
        {
            $detailText .= GetMessage("articul_checklist_COMMON_SITE_CHECK", array(
                "#SITE#" => $arSite["ID"]
            ));
            $detailText .= "<ul>";

            if (!$arSite["SERVER_NAME"]) {
                $bSuccess = false;
                $detailText .= "<li>" . GetMessage("articul_checklist_SERVER_NAME_NOT_FOUND")
                    . "<a target=\"_blank\" " . "href=\"/bitrix/admin/site_edit.php?LID=" . $arSite['LID'] . "\">"
                    . GetMessage("articul_checklist_SERVER_URL") . "</a></span>" . "</li>";
                $detailText .= "</ul>";
                continue;
            }

            $siteUrl = "http://" . $arSite["SERVER_NAME"] . $arSite["DIR"];

            $headers = @get_headers($siteUrl);
            if (strpos($headers[0],'200') == false) {
                $bSuccess = false;
                $detailText .= "<li>" . GetMessage("articul_checklist_DOMEN")
                    . "&nbsp;<a target=\"_blank\" " . "href=\"$siteUrl\">"
                    . $siteUrl . "</a>&nbsp;" . GetMessage("articul_checklist_NOT_FOUND")  . "</span>" . "</li>";
                $detailText .= "</ul>";
                continue;
            }

            $detailText .= "<li>".GetMessage("articul_checklist_COMMON_URL_CHECK", array(
                "#URL#" => $siteUrl
            ))."</li>";

            $mainPageContent = file_get_contents($siteUrl);
            if (!$mainPageContent)
            {
                $bSuccess = false;
                $detailText .= "<li>".GetMessage("articul_checklist_COMMON_PAGE_NOT_AVAILABLE")."</li>";
                $detailText .= "</ul>";
                continue;
            }

            preg_match_all('/<html lang(\s)?=(\s)?"[a-z]{2}">/s', $mainPageContent, $matches);
            if ($matches[0][0])
            {
                $detailText .= "<li>".GetMessage("articul_checklist_AM.CONT.004_TAG_FOUND")."</li>";
            }
            else
            {
                $bSuccess = false;
                $detailText .= "<li>".GetMessage("articul_checklist_AM.CONT.004_TAG_NOT_FOUND")."</li>";
            }

            $detailText .= "</ul>";
        }

        $detailText .= "<p>".GetMessage("articul_checklist_AM.CONT.004_MORE_INFO")."</p>";

        if (!$bSuccess)
        {
            return static::GetFailedResult(array(
                "DETAIL" => $detailText
            ));
        }

        return static::GetSuccessResult(array(
            "DETAIL" => $detailText
        ));
    }

    static public function CheckContentOgTags()
    {
        $arSites = static::GetSites();
        $bSuccess = true;
        $detailText = "";

        foreach ($arSites as $arSite)
        {
            $detailText .= GetMessage("articul_checklist_COMMON_SITE_CHECK", array(
                "#SITE#" => $arSite["ID"]
            ));
            $detailText .= "<ul>";

            if (!$arSite["SERVER_NAME"]) {
                $bSuccess = false;
                $detailText .= "<li>" . GetMessage("articul_checklist_SERVER_NAME_NOT_FOUND")
                    . "<a target=\"_blank\" " . "href=\"/bitrix/admin/site_edit.php?LID=" . $arSite['LID'] . "\">"
                    . GetMessage("articul_checklist_SERVER_URL") . "</a></span>" . "</li>";
                $detailText .= "</ul>";
                continue;
            }

            $siteUrl = "http://".$arSite["SERVER_NAME"].$arSite["DIR"];

            $headers = @get_headers($siteUrl);
            if (strpos($headers[0],'200') == false) {
                $bSuccess = false;
                $detailText .= "<li>" . GetMessage("articul_checklist_DOMEN")
                    . "&nbsp;<a target=\"_blank\" " . "href=\"$siteUrl\">"
                    . $siteUrl . "</a>&nbsp;" . GetMessage("articul_checklist_NOT_FOUND")  . "</span>" . "</li>";
                $detailText .= "</ul>";
                continue;
            }

            $detailText .= "<li>".GetMessage("articul_checklist_COMMON_URL_CHECK", array(
                "#URL#" => $siteUrl
            ))."</li>";

            $mainPageContent = file_get_contents($siteUrl);
            preg_match_all('/<head(.*)<\/head>/s', $mainPageContent, $matchesHeader);
            $mainPageHeader = $matchesHeader[1][0];

            if (!$mainPageHeader)
            {
                $bSuccess = false;
                $detailText .= "<li>".GetMessage("articul_checklist_COMMON_PAGE_NOT_AVAILABLE")."</li>";
                $detailText .= "</ul>";
                continue;
            }

            $arTags = array(
                "og:title",
                "og:description",
                "og:type",
                "og:url",
                "og:image",
            );
            foreach ($arTags as $tag)
            {
                $matches = false;
                preg_match_all("/<meta.*?property=\"$tag\".*?content=\"(.*?)\"/s", $mainPageHeader, $matches);
                $content = trim($matches[1][0]);
                if ($content)
                {
                    $detailText .= "<li>".GetMessage("articul_checklist_AM.CONT.005_TAG_FOUND", array(
                        "#TAG#" => $tag
                    ))."</li>";
                }
                else
                {
                    $bSuccess = false;
                    $detailText .= "<li>".GetMessage("articul_checklist_AM.CONT.005_TAG_NOT_FOUND", array(
                        "#TAG#" => $tag
                    ))."</li>";
                }
            }

            $detailText .= "</ul>";
        }

        if (!$bSuccess)
        {
            $detailText .= "<p>".GetMessage("articul_checklist_AM.CONT.005_MORE_INFO")."</p>";
            return static::GetFailedResult(array(
                "DETAIL" => $detailText
            ));
        }

        return static::GetSuccessResult(array(
            "DETAIL" => $detailText
        ));
    }

    static public function CheckSecurityGitConfigAccess()
    {
        $gitConfigUrl = "http://".$_SERVER["HTTP_HOST"]."/.git/config";
        $responseHeaders = @get_headers($gitConfigUrl);
        if (strpos($responseHeaders[0], "200 OK") !== false)
        {
            return static::GetFailedResult(array(
                "DETAIL" => GetMessage("articul_checklist_AM.SECR.001_FAIL", array(
                    "#URL#" => $gitConfigUrl
                ))
            ));
        }

        return static::GetSuccessResult();
    }

    static public function CheckArchFolderLocal()
    {
        $arSites = static::GetSites();
        $bSuccess = true;
        $detailText = "";

        foreach ($arSites as $arSite)
        {
            $detailText .= GetMessage("articul_checklist_COMMON_SITE_CHECK", array(
                "#SITE#" => $arSite["ID"]
            ));
            $detailText .= "<ul>";

            $folderPath = $arSite["ABS_DOC_ROOT"].$arSite["DIR"]."local/";
            if (file_exists($folderPath))
            {
                $detailText .= "<li>".GetMessage("articul_checklist_COMMON_FOLDER_FOUND", array(
                    "#FOLDER#" => $folderPath
                ))."</li>";
            }
            else
            {
                $bSuccess = false;
                $detailText .= "<li>".GetMessage("articul_checklist_COMMON_FOLDER_NOT_FOUND", array(
                    "#FOLDER#" => $folderPath
                ))."</li>";
            }

            $detailText .= "</ul>";
        }

        if (!$bSuccess)
        {
            return static::GetFailedResult(array(
                "DETAIL" => $detailText
            ));
        }

        return static::GetSuccessResult(array(
            "DETAIL" => $detailText
        ));
    }

    static public function CheckHostingGitInstalled()
    {
        $shellResponse = shell_exec("git help");
        if (strpos($shellResponse, "usage: git") === false)
        {
            return static::GetFailedResult(array(
                "DETAIL" => GetMessage("articul_checklist_AM.HOST.101_GIT_NOT_FOUND", array(
                    "#RESPONSE#" => $shellResponse
                ))
            ));
        }

        return static::GetSuccessResult();
    }

    static public function CheckCommonAutoBackupEnabled()
    {
        if (!\CModule::IncludeModule("bitrixcloud"))
        {
            return static::GetFailedResult(array(
                "DETAIL" => GetMessage("articul_checklist_AM.COMN.104_BITRIXCLOUD_NOT_FOUND")
            ));
        }

        $arJobs = \CBitrixCloudBackup::getInstance()->getBackupJob();
        if (!$arJobs[0]["URL"] || !$arJobs[0]["TIME"])
        {
            return static::GetFailedResult(array(
                "DETAIL" => GetMessage("articul_checklist_COMMON_MODULE_NOT_FOUND", array(
                    "#MODULE#" => "bitrixcloud"
                ))
            ));
        }

        return static::GetSuccessResult();
    }

    static public function CheckCommonMonitoringEnabled()
    {
        if (!\CModule::IncludeModule("bitrixcloud"))
        {
            return static::GetFailedResult(array(
                "DETAIL" => GetMessage("articul_checklist_COMMON_MODULE_NOT_FOUND", array(
                    "#MODULE#" => "bitrixcloud"
                ))
            ));
        }

        $arSites = static::GetSites();
        $arDomains = \CBitrixCloudMonitoring::getInstance()->getConfiguredDomains();

        $bSuccess = true;
        $detailText = "";

        foreach ($arSites as $arSite)
        {
            $detailText .= GetMessage("articul_checklist_COMMON_SITE_CHECK", array(
                "#SITE#" => $arSite["ID"]
            ));
            $detailText .= "<ul>";

            $detailText .= "<li>" . GetMessage("articul_checklist_COMMON_URL_CHECK", array(
                "#URL#" => $arSite["SERVER_NAME"]
            )) . "</li>";

            if ($arDomains[$arSite["SERVER_NAME"]])
            {
                $detailText .= "<li>" . GetMessage("articul_checklist_AM.COMN.105_MONITORING_ENABLED") . "</li>";
            }
            else
            {
                $bSuccess = false;
                $detailText .= "<li>" . GetMessage("articul_checklist_AM.COMN.105_MONITORING_DISABLED") . "</li>";
            }

            $detailText .= "</ul>";
        }

        if (!$bSuccess)
        {
            $detailText .= GetMessage("articul_checklist_AM.COMN.105_MORE_INFO");
            return static::GetFailedResult(array(
                "DETAIL" => $detailText
            ));
        }

        return static::GetSuccessResult(array(
            "DETAIL" => $detailText
        ));
    }
}